﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
internal class Program
{
    
    static void Main(string[] args)
    {
        RandomNumberGenerator a = new RandomNumberGenerator();
        a.test();
        string menu = "";
        int sequenceLength = 10;
        int[] numberSequence = new int[sequenceLength];
        while (menu != "exit")
        {
            Console.WriteLine("a: véletlenszám sorozat generálás b: cserés rendezéssel rendezés c: minimumkiválasztás rendezés d: fájlba ír");
            Console.Write("Választott menü: ");
            
            
            menu = Console.ReadLine();
            switch (menu)
            {
                case "a":
                    // Véletlenszám sorozat generálása
                    int[] sequence = a.GenerateRandomNumberSequence(sequenceLength, 1, 100);

                    // A sorozat kiírása a konzolra
                    Console.WriteLine("Véletlenszám sorozat:");
                    foreach (int number in sequence)
                    {
                        Console.Write(number + " ");
                    }
                    Console.WriteLine();
                    numberSequence = sequence;
                    break;
                case "b":
                    try
                    {
                        //Egyszerű cserés rendezés
                        for(int i = 0; i < numberSequence.Length - 1; i++)
                        {
                            for(int j = i + 1; j < numberSequence.Length; j++)
                            {
                                if (numberSequence[j] < numberSequence[i])
                                {
                                    int temp = numberSequence[i];
                                    numberSequence[i] = numberSequence[j];
                                    numberSequence[j] = temp;
                                }
                            }
                        }
                        //Sorozat kiírása a rendezés után
                        Console.WriteLine("Sorozat egyszerű cserés rendezéssel rendezve:");
                        foreach (int number in numberSequence)
                        {
                            Console.Write(number + " ");
                        }
                        Console.WriteLine();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Hiba történt a rendezés folyamán.");
                    }
                    break;
                case "c":
                    try
                    {
                        //Minimumkiválasztásos rendezés
                        for (int i = 0; i < numberSequence.Length; i++)
                        {
                            int min = i;
                            for (int j = i + 1; j < numberSequence.Length; j++)
                            {
                                if (numberSequence[j] < numberSequence[min])
                                {
                                    min = j;
                                }
                            }
                            int temp = numberSequence[i];
                            numberSequence[i] = numberSequence[min];
                            numberSequence[min] = temp;
                        }
                        //Sorozat kiírása a rendezés után
                        Console.WriteLine("Sorozat minimumkiválasztásos rendezéssel rendezve:");
                        foreach (int number in numberSequence)
                        {
                            Console.Write(number + " ");
                        }
                        Console.WriteLine();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Hiba történt a rendezés folyamán.");
                    }
                    break;
                case "d":
                    try
                    {
                        //Fájl létrehozás
                        using (StreamWriter outputFile = new StreamWriter(@".\numbers.txt"))
                        {
                            //Fájl feltöltése a sorozattal
                            foreach (int number in numberSequence)
                            {
                                outputFile.Write(number + " ");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //Jogosultsági problémát külön kezeljük, a többinél csak hibát jelzünk
                        if (e is UnauthorizedAccessException)
                        {
                            Console.WriteLine("Nincs jogosultságod fájl írására.");
                        }
                        else
                        {
                            Console.WriteLine("Hiba történt a fájl írása folyamán.");
                        }
                    }
                    
                    break;
                case "exit":
                    break;
            }

        }
    }

}
