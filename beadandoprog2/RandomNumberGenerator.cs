﻿using System;

    public partial class RandomNumberGenerator
    {
        private static Random random = new Random();

        public static int GenerateRandomNumber(int minValue, int maxValue)
        {
            return random.Next(minValue, maxValue + 1);
        }
        public void test()
        {
            Console.WriteLine("a");
        }
    public int[] GenerateRandomNumberSequence(int count, int minValue, int maxValue)
    {
        int[] sequence = new int[count];

        for (int i = 0; i < count; i++)
        {
            sequence[i] = GenerateRandomNumber(minValue, maxValue);
        }

        return sequence;
    }
}

